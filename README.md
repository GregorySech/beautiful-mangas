# BeautifulMangas

My opensource manga browser based on the beautifulsoup port for Dart.
I've made this app because I really do not like the apps currently
available in the Google Play Store to read mangas so I decided to do mine,
I'm open sourcing this just as a Flutter app example that integrates
a lot of interesting libraries which I want to get accustomed to.
I have no intention of publishing this app on any store and I'd like
no one to do so. The only future I see for this app is if some website
that is compatible with it decides to publish it, would that be the
case I'd really like a shoutout.

## Todo
- Making FeedPage beautiful.
- Making DetailsPage beautiful.
- Making ChapterReader beautiful.
- Implementing Bookmarks (Sembast).
- Offline cacheing (again, Sembast).
- Better navigation.

## Hobby's contributions
As you might have seen the app is quite ugly. I know nothing of design,
everyone is free to contribute. I'll kind of wing it when deciding if
a design will be kept or not.
Even if you are not a designer feel free to contribute, this is a hobby
project and do not have much expectations for it. I just wanted to try
a couple of new libraries.


