import 'package:beautifulmangas/businesslogic.dart';
import 'package:beautifulmangas/commons.dart';
import 'package:beautifulmangas/details/details.dart';
import 'package:beautifulmangas/feed/preview.dart';
import 'package:beautifulmangas/models.dart';
import 'package:flutter/material.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';
import 'package:provider/provider.dart';

export 'package:beautifulmangas/feed/preview.dart';

/*
Pagina con il feed dei manga su FanFox, in cima ha un indicatore per quando
scatta una richiesta per la pagina successiva del feed.
 */
class FeedPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          StreamBuilder<bool>(
            stream: Provider.of<WebPageBLoC>(context).loading,
            builder: (context, snapshot) {
              if (snapshot.hasData && !snapshot.data) return Container();
              return LinearProgressIndicator();
            },
          ),
          Expanded(child: MangaBrowser()),
        ],
      ),
    );
  }
}

/*
Griglia contenente le preview dei vari manga.
 */
class MangaBrowser extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<MangaPreview>>(
      stream: Provider.of<WebPageBLoC>(context).previews,
      builder:
          (BuildContext context, AsyncSnapshot<List<MangaPreview>> snapshot) {
        if (snapshot.hasData) {
          // dei dati sono presenti nel feed, renderizziamoli!
          final items = snapshot.data;
          /*
          LazyLoadScrollView ha una callback che mi permette di far partire una
          richiesta per le prossime pagine,
          TODO lavorare su scrollOffset
          TODO gestire mancato caricamento dei dati e/o mancanza di connessione.
           */
          return LazyLoadScrollView(
              scrollOffset: (MediaQuery.of(context).size.height * 5).ceil(),
              child: GridView.builder(
                itemCount: items.length,
                gridDelegate: gridSliverDelegateBuilder(context),
                itemBuilder: (context, index) {
                  return Card(
                    child: InkWell(
                      onTap: () {
                        Provider.of<MangaBLoC>(context)
                            .prepare
                            .add(items[index]);
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) =>
                                MangaDetailsPage(items[index]),
                          ),
                        );
                      },
                      child:
                          MangaPreviewCell.fromPreview(items[index], context),
                    ),
                  );
                },
              ),
              onEndOfPage: () {
                Provider.of<WebPageBLoC>(context).nextPage();
              });
        }
        /*
        Non ci sono dati nello stream, ecco delle preview vuote.
         */
        return GridView.builder(
          gridDelegate: gridSliverDelegateBuilder(context),
          itemBuilder: (context, index) {
            return Card(child: MangaPreviewCell());
          },
        );
      },
    );
  }
}
