import 'package:beautifulmangas/commons.dart';
import 'package:beautifulmangas/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:flutter_advanced_networkimage/transition.dart';

class MangaPreviewCell extends StatelessWidget {
  final Widget image;
  final Widget title;
  final Widget body;

  MangaPreviewCell({
    this.image,
    this.title,
    this.body,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Expanded(
            flex: 5,
            child: Padding(
              padding: const EdgeInsets.all(4),
              child: image ?? MyShimmer(),
            ),
          ),
          Expanded(
            flex: 3,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Expanded(
                  flex: 4,
                  child: Padding(
                    padding: const EdgeInsets.all(4),
                    child: title ?? MyShimmer(),
                  ),
                ),
                Expanded(
                  flex: 5,
                  child: Padding(
                    padding: const EdgeInsets.all(4),
                    child: body ?? MyShimmer(),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  factory MangaPreviewCell.fromPreview(MangaPreview preview, BuildContext context) {
    return MangaPreviewCell(
      image: SizedBox.expand(
        child: Padding(
          padding: const EdgeInsets.all(4),
          child: Hero(
            tag: preview.title,
            child: TransitionToImage(
              borderRadius: BorderRadius.circular(4),
              fit: BoxFit.cover,
              image: AdvancedNetworkImage(
                preview.previewUrl.toString(),
                useDiskCache: true,
                cacheRule: CacheRule(),
              ),
              loadingWidget: MyShimmer(),
            ),
          ),
        ),
      ),
      title: Center(
        child: Text(
          preview.title,
          style: Theme.of(context).textTheme.subhead,
          maxLines: 1,
          overflow: TextOverflow.fade,
        ),
      ),
      body: Row(
        children: <Widget>[
          IconButton(
              icon: Icon(Icons.favorite_border),
              onPressed: () {
                //TODO implementare salvataggio dei preferiti
              }),
          Expanded(
            child: FittedBox(
              fit: BoxFit.contain,
              child: Chip(
                label: Text(
                  preview.lastChapter,
                  overflow: TextOverflow.fade,
                ),
              ),
            ),
          ),
        ],
        mainAxisSize: MainAxisSize.max,
      ),
    );
  }
}
