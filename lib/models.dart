class Manga {
  final String title;
  final String status;
  final num vote;
  final Uri link;
  final List<MangaChapter> chapters;
  final List<MangaTag> tags;
  final Uri coverUrl;
  final String author;
  final String details;

  Manga({
    this.title,
    this.status,
    this.vote,
    this.link,
    this.chapters,
    this.tags,
    this.coverUrl,
    this.author,
    this.details,
  });

  Manga enrich(Manga withThis) => Manga(
        title: this.title ?? withThis.title,
        status: this.status ?? withThis.status,
        vote: this.vote ?? withThis.vote,
        link: this.link ?? withThis.link,
        chapters: this.chapters ?? withThis.chapters,
        tags: this.tags ?? withThis.tags,
        coverUrl: this.coverUrl ?? withThis.coverUrl,
        author: this.author ?? withThis.author,
        details: this.details ?? withThis.details,
      );
}

class MangaTag {
  final String name;
  final Uri link;

  MangaTag(
    this.name,
    this.link,
  );
}

class MangaChapter {
  final Uri mangaLink;
  final String title;
  final Uri chapterUrl;

  MangaChapter(
    this.mangaLink,
    this.title,
    this.chapterUrl,
  );
}

class Webpage {
  final List<MangaPreview> items;
  final Uri previousPage;
  final Uri nextPage;

  Webpage(
    this.items,
    this.previousPage,
    this.nextPage,
  );
}

class MangaPreview {
  final String title;
  final String lastChapter;
  final Uri previewUrl;
  final Uri link;
  final Uri lastChapterUrl;

  MangaPreview({
    this.title,
    this.link,
    this.previewUrl,
    this.lastChapter,
    this.lastChapterUrl,
  });
}
