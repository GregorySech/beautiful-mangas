import 'dart:math';

import 'package:beautifulmangas/businesslogic.dart';
import 'package:beautifulmangas/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:groovin_material_icons/groovin_material_icons.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';

class MangaDetailsPage extends StatelessWidget {
  final MangaPreview preview;

  MangaDetailsPage(this.preview);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            centerTitle: true,
            pinned: true,
            title: Center(
              child: Text(
                preview.title,
              ),
            ),
            expandedHeight: MediaQuery
                .of(context)
                .size
                .height / pi,
            flexibleSpace: FlexibleSpaceBar(
              background: Hero(
                tag: preview.title,
                child: Image(
                  image: AdvancedNetworkImage(preview.previewUrl.toString(),
                      useDiskCache: true),
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
          StreamBuilder<Manga>(
              stream: Provider
                  .of<MangaBLoC>(context)
                  .manga,
              builder: (context, snapshot) {
                if (snapshot.hasData && snapshot.data.chapters != null) {
                  return SliverGrid(
                    delegate: SliverChildBuilderDelegate(
                          (context, index) =>
                          Card(
                            child: InkWell(
                              onTap: () {
                                Provider
                                    .of<MangaBLoC>(context)
                                    .reading
                                    .add(index);
                                Navigator.of(context).pushNamed("/reader");
                              },
                              child: MangaChapterPreviewCell.fromChapter(
                                snapshot.data.chapters[index],
                                context,
                              ),
                            ),
                          ),
                      childCount: snapshot.data.chapters.length,
                    ),
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      childAspectRatio: pi,
                    ),
                  );
                }
                return SliverGrid(
                  delegate: SliverChildBuilderDelegate(
                        (context, item) =>
                        Card(child: MangaChapterPreviewCell()),
                  ),
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    childAspectRatio: pi,
                  ),
                );
              }),
        ],
      ),
    );
  }
}

class MangaChapterPreviewCell extends StatelessWidget {
  final Widget title;
  final Widget read;

  MangaChapterPreviewCell({
    this.title,
    this.read,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Expanded(
            flex: 5,
            child: Padding(
              padding: const EdgeInsets.all(4),
              child: title ?? shimmer(),
            ),
          ),
          Expanded(
            flex: 2,
            child: read ?? shimmer(),
          ),
        ],
      ),
    );
  }

  static Widget shimmer() {
    return Shimmer.fromColors(
      child: Card(
        color: Colors.transparent,
        child: Container(),
      ),
      baseColor: Colors.grey[850],
      highlightColor: Colors.grey[800],
    );
  }

  factory MangaChapterPreviewCell.fromChapter(
    MangaChapter chapter,
    BuildContext context,
  ) {
    return MangaChapterPreviewCell(
      read: Icon(GroovinMaterialIcons.book_open),
      title: Padding(
        padding: const EdgeInsets.all(4),
        child: Center(
          child: Text(
            chapter.title,
            maxLines: 2,
            overflow: TextOverflow.fade,
          ),
        ),
      ),
    );
  }
}
