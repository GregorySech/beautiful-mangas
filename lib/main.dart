import 'package:beautifulmangas/businesslogic.dart';
import 'package:beautifulmangas/feed/feed.dart';
import 'package:beautifulmangas/reader/reader.dart';
import 'package:beautifulmangas/services.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:provider/provider.dart';

void main() {
  /*
  GetIt è un service locator per non far sapere a chi usa un servizio
  le dipendenze del servizio stesso, al momento è un po' overkill ma
  più avanti sarà utile per spezzare le dipendenze tra componenti.
  */
  final getIt = new GetIt();
  getIt.registerLazySingleton<Dio>(() {
    final dio = new Dio();
    dio.interceptors
        .add(LogInterceptor(requestHeader: false, responseHeader: false));
    return dio;
  });
  getIt.registerLazySingleton<MangaScraper>(
      () => MangaScraper("http://mangafox.pro"));
  getIt.registerLazySingleton<DataService>(
    () => DataService("http://mangafox.pro", getIt),
  );

  /*
  Stateful Provider è l'implementazione di Provider che mi serve per i vari
  BLoC che andranno a comporre l'app. Espone anche una callback per fare
  pulizia ma al momento non è necessario.
   */
  runApp(
    StatefulProvider(
      child: StatefulProvider(
        child: MyApp(),
        valueBuilder: (_) => WebPageBLoC(getIt)..nextPage(),
      ),
      valueBuilder: (_) => MangaBLoC(getIt),
    ),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Beautiful Mangas',
      theme: ThemeData.dark().copyWith(
        accentColor: Colors.grey[500],
      ),
      home: FeedPage(),
      routes: {
        "/reader": (_) => MangaReaderPage(),
      },
    );
  }
}
