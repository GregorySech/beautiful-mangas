import 'package:beautifulmangas/businesslogic.dart';
import 'package:beautifulmangas/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:flutter_advanced_networkimage/transition.dart';
import 'package:flutter_advanced_networkimage/zoomable.dart';
import 'package:groovin_material_icons/groovin_material_icons.dart';
import 'package:provider/provider.dart';

class MangaReaderPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StreamBuilder<List<Uri>>(
          stream: Provider.of<MangaBLoC>(context).pages,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return Reader(snapshot.data);
            }
            return Center(
              child: CircularProgressIndicator(),
            );
          }),
      bottomNavigationBar: BottomAppBar(
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            StreamBuilder<int>(
                stream: Provider
                    .of<MangaBLoC>(context)
                    .previousChapter,
                builder: (context, snapshot) {
                  return IconButton(
                      icon: Icon(GroovinMaterialIcons.arrow_left),
                      onPressed: snapshot.hasData
                          ? () {
                        Provider
                            .of<MangaBLoC>(context)
                            .reading
                            .add(snapshot.data);
                      }
                          : null);
                }),
            Expanded(
              child: Hero(
                tag: "title",
                child: StreamBuilder<MangaChapter>(
                    stream: Provider
                        .of<MangaBLoC>(context)
                        .chapter,
                    builder: (context, snapshot) {
                      if (snapshot.hasData)
                        return FittedBox(
                          fit: BoxFit.scaleDown,
                          child: Center(
                            child: Text(
                              "Chapter: ${snapshot.data.title}",
                            ),
                          ),
                        );
                      return CircularProgressIndicator();
                    }),
              ),
            ),
            StreamBuilder<int>(
                stream: Provider.of<MangaBLoC>(context).nextChapter,
                builder: (context, snapshot) {
                  return IconButton(
                      icon: Icon(GroovinMaterialIcons.arrow_right),
                      onPressed: snapshot.hasData
                          ? () {
                        Provider
                            .of<MangaBLoC>(context)
                            .reading
                            .add(snapshot.data);
                      }
                          : null);
                })
          ],
        ),
      ),
    );
  }
}

class Reader extends StatefulWidget {
  final List<Uri> pages;

  Reader(this.pages, {Key key}) : super(key: key);

  @override
  _ReaderState createState() => _ReaderState();
}

class _ReaderState extends State<Reader> {
  int currentPage;

  @override
  void initState() {
    super.initState();
    currentPage = 0;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Expanded(
          child: PageView.builder(
            itemCount: widget.pages.length,
            itemBuilder: (context, index) {
              return ReaderPageWidget(widget.pages[index]);
            },
            onPageChanged: (int page) {
              setState(() {
                currentPage = page;
              });
            },
          ),
        ),
        LinearProgressIndicator(
          value: (currentPage + 1) / widget.pages.length,
        ),
      ],
    );
  }
}

class ReaderPageWidget extends StatefulWidget {
  final Uri url;

  ReaderPageWidget(this.url, {Key key}) : super(key: key);

  @override
  _ReaderPageWidgetState createState() => _ReaderPageWidgetState();
}

class _ReaderPageWidgetState extends State<ReaderPageWidget> {
  ImageProvider provider;
  double loaded;

  @override
  void initState() {
    super.initState();
    provider = AdvancedNetworkImage(
      widget.url.toString(),
      loadFailedCallback: () {
        print("Image loading failed for : ${widget.url.toString()}");
      },
      useDiskCache: true,
      loadingProgress: (value) {
        setState(() {
          loaded = value;
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return ZoomableWidget(
      minScale: 1,
      maxScale: 4,
      zoomSteps: 2,
      autoCenter: true,
      bounceBackBoundary: true,
      child: Container(
        child: TransitionToImage(
            loadingWidget: Center(
              child: CircularProgressIndicator(
                value: loaded,
              ),
            ),
            image: provider),
      ),
    );
  }
}
