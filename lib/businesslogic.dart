import 'dart:async';

import 'package:beautifulmangas/models.dart';
import 'package:beautifulmangas/services.dart';
import 'package:get_it/get_it.dart';
import 'package:rxdart/rxdart.dart';

class WebPageBLoC {
  final GetIt getIt;

  final BehaviorSubject<Webpage> _page;
  final BehaviorSubject<List<MangaPreview>> _previews;
  final BehaviorSubject<bool> _endOfFeed;
  final BehaviorSubject<bool> _loading;

  WebPageBLoC(this.getIt)
      : _page = BehaviorSubject(),
        _previews = BehaviorSubject(),
        _endOfFeed = BehaviorSubject.seeded(false),
        _loading = BehaviorSubject.seeded(false) {
    _page.listen((webpage) {
      final items =
          (<MangaPreview>[]..addAll(_previews.value ?? <MangaPreview>[]))
            ..addAll(webpage.items);
      _previews.add(items);
    });
  }

  Observable<List<MangaPreview>> get previews => _previews.asBroadcastStream();

  Observable<bool> get loading => _loading.asBroadcastStream();

  nextPage() async {
    var pageUrl = null;
    if (_page.value == null) {
      pageUrl = this.getIt.get<DataService>().defaultWebpageUrl;
    } else {
      if (_page.value.nextPage == null) {
        _endOfFeed.add(true);
      } else {
        pageUrl = _page.value.nextPage;
      }
    }
    _loading.add(true);
    _page.add(await this.getIt.get<DataService>().retrivePage(pageUrl));
    _loading.add(false);
  }
}

class MangaBLoC {
  final GetIt getIt;
  final BehaviorSubject<MangaPreview> _preview;
  final BehaviorSubject<Manga> _manga;
  final BehaviorSubject<List<Uri>> _chapterPages;
  final BehaviorSubject<MangaChapter> _currentChapter;
  final BehaviorSubject<int> _currentChapterPosition;
  final BehaviorSubject<int> _nextChapterPosition;
  final BehaviorSubject<int> _previousChapterPosition;

  StreamSubscription<MangaPreview> _subscription;
  StreamSubscription<int> _chapterSubscription;

  MangaBLoC(this.getIt)
      : this._preview = BehaviorSubject(),
        this._manga = BehaviorSubject(),
        this._currentChapterPosition = BehaviorSubject(),
        this._chapterPages = BehaviorSubject(),
        this._currentChapter = BehaviorSubject(),
        this._nextChapterPosition = BehaviorSubject(),
        this._previousChapterPosition = BehaviorSubject() {
    _subscription = _preview.listen(_loadManga);
    _chapterSubscription = _currentChapterPosition.listen(_loadChapter);
  }

  _loadChapter(int event) async {
    _chapterPages.add(null);
    _currentChapter.add(null);
    _nextChapterPosition.add(null);
    _previousChapterPosition.add(null);
    if (event != null && event >= 0) {
      final m = _manga?.value;
      if (m != null && m.chapters != null && m.chapters.length > event) {
        if (event > 0) {
          _previousChapterPosition.add(event - 1);
        }
        if (event < m.chapters.length - 1) {
          _nextChapterPosition.add(event + 1);
        }
        _currentChapter.add(m.chapters[event]);
        final pages = await getIt
            .get<DataService>()
            .retrieveChapterPages(m.chapters[event].chapterUrl);
        if (_manga.value != null) {
          _chapterPages.add(pages);
        }
      }
    }
  }

  _loadManga(MangaPreview event) async {
    if (event != null) {
      _manga.add(
        Manga(
          title: event.title,
          link: event.link,
          coverUrl: event.previewUrl,
        ),
      );
      final retrieved = await getIt.get<DataService>().retriveManga(event.link);
      if (_preview.value != null) {
        _manga.add(retrieved);
      }
    } else {
      // Se ricevo una preview nulla resetto la business logic.
      _manga.add(null);
    }
  }

  Observable<List<Uri>> get pages => _chapterPages.asBroadcastStream();

  Observable<Manga> get manga => _manga.asBroadcastStream();

  Observable<MangaChapter> get chapter => _currentChapter.asBroadcastStream();

  Observable<int> get currentChapter =>
      _currentChapterPosition.asBroadcastStream();

  Observable<int> get previousChapter =>
      _previousChapterPosition.asBroadcastStream();

  Observable<int> get nextChapter => _nextChapterPosition.asBroadcastStream();

  Sink<MangaPreview> get prepare => _preview.sink;

  Sink<int> get reading => _currentChapterPosition.sink;

  dispose() {
    _subscription.cancel();
    _chapterSubscription.cancel();
  }
}
