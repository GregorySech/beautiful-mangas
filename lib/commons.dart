import 'dart:math';

import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

/*
  Se il device è in portrait 2 colonne altrimenti (landscape) 4
   */
int _gridCrossAxisCount(BuildContext context) {
  return MediaQuery.of(context).orientation == Orientation.portrait ? 2 : 4;
}

gridSliverDelegateBuilder(BuildContext context) => SliverGridDelegateWithFixedCrossAxisCount(
    crossAxisCount: _gridCrossAxisCount(context),
    childAspectRatio: 2 / pi);


class MyShimmer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      child: Card(
        color: Colors.transparent,
        child: Container(),
      ),
      baseColor: Colors.grey[700],
      highlightColor: Colors.grey[500],
    );
  }
}
