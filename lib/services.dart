import 'package:beautifulmangas/models.dart';
import 'package:beautifulsoup/beautifulsoup.dart';
import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';

class DataService {
  final String root;
  final GetIt getIt;

  DataService(this.root, this.getIt);

  Uri get defaultWebpageUrl => Uri.parse("$root/popular-manga/");

  Future<Webpage> retrivePage(Uri url) async {
    final response = await getIt.get<Dio>().get<String>(url.toString());
    return getIt.get<MangaScraper>().scrapeWebpage(response.data);
  }

  Future<Manga> retriveManga(Uri mangaUrl) async {
    final mangaPromise =
        getIt.get<Dio>().get<String>(mangaUrl.toString()).then((response) {
      return getIt.get<MangaScraper>().scrapeFromMangaPage(response.data);
    });

    return await mangaPromise;
  }

  Future<List<Uri>> retrieveChapterPages(Uri chapterUrl) async {
    final response =
        await getIt.get<Dio>().get<String>("${chapterUrl.toString()}/0");
    return getIt.get<MangaScraper>().scrapeChapterPages(response.data);
  }
}

class MangaScraper {
  final String root;

  MangaScraper(this.root);

  MangaPreview scrapePreview(String previewBody) {
    final soup = Beautifulsoup(previewBody);
    return MangaPreview(
        lastChapter: soup.find_all("h3 + a").first.text,
        lastChapterUrl:
            Uri.parse(soup.find_all("h3 + a").first.attributes["href"]),
        title: soup.find(id: "h3").text.trim(),
        link: Uri.parse(soup.find_all("h3 a").first.attributes["href"]),
        previewUrl: Uri.parse(soup.find_all("a img").first.attributes["src"])
            .replace(scheme: "http"));
  }

  Uri _validateReference(String subdir) {
    if (subdir == "javascript:void(0)") return null;
    return Uri.parse(subdir);
  }

  Webpage scrapeWebpage(String pageBody) {
    final soup = Beautifulsoup(pageBody);

    final pagination = soup.find_all(".pagination li a");
    var next = pagination.last.attributes["href"];
    var previous = pagination.first.attributes["href"];
    final previews = soup
        .find_all(".truyen-list .list-truyen-item-wrap")
        .map<MangaPreview>((element) => this.scrapePreview(element.outerHtml))
        .toList();
    return Webpage(
      previews,
      _validateReference(previous),
      _validateReference(next),
    );
  }

  Manga scrapeFromMangaPage(String pageBody) {
    final soup = Beautifulsoup(pageBody);
    final previewUrl =
        soup.find_all(".manga-info-pic img").first.attributes["src"];
    final title = soup.find_all(".manga-info-text h1").first.text;
    final infos = soup.find_all(".manga-info-text li");
    final status = infos[2].text.trim();
    final author = infos[1].text.trim();
    final tags = soup
        .find_all(".manga-info-text li a")
        .map<MangaTag>(
          (element) =>
              MangaTag(element.text, Uri.parse(element.attributes["href"])),
        )
        .toList();
    final detail = soup.find_all(".manga-info-top + div").first.text.trim();
    final chapters = soup
        .find_all(".chapter-list a")
        .map<MangaChapter>(
            (e) => MangaChapter(null, e.text, Uri.parse(e.attributes["href"])))
        .toList()
        .reversed
        .toList();

    return Manga(
      title: title,
      coverUrl: Uri.parse(previewUrl),
      status: status,
      tags: tags,
      author: author,
      details: detail,
      chapters: chapters,
    );
  }

  @deprecated
  Manga scrapeFromChapterPage(String pageBody) {
    final soup = Beautifulsoup(pageBody);
    final mangaLink = _validateReference(
        soup.find_all(".reader-header-title-1 a").first.attributes["href"]);
    final chapters = soup
        .find_all(".reader-header-title-list a")
        .map<MangaChapter>(
          (element) =>
          MangaChapter(
            mangaLink,
            element.text,
            _validateReference(element.attributes["href"]),
          ),
    )
        .toList();
    return Manga(link: mangaLink, chapters: chapters);
  }

  List<Uri> scrapeChapterPages(String chapterBody) {
    final soup = Beautifulsoup(chapterBody);
    return soup
        .find_all(".vung-doc img")
        .map<Uri>((element) => Uri.parse(element.attributes["src"]))
        .toList();
  }
}
