import 'package:beautifulmangas/services.dart';
import 'package:beautifulsoup/beautifulsoup.dart';
import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  Dio client;
  MangaScraper parser;

  final root = "http://mangafox.pro";
  setUpAll(() {
    client = new Dio();
    parser = new MangaScraper(root);
  });

  test("Poking around with BeautifulSoup", () async {
    final response = await client.get<String>("$root/popular-manga");
    expect(response, isNotNull);
    expect(response.statusCode, equals(200));

    final soup = Beautifulsoup(response.data);
    expect(soup, isNotNull);

    final webpage = parser.scrapeWebpage(response.data);
    expect(webpage, isNotNull);
    expect(webpage.items, isNotNull);
    expect(webpage.items, isNotEmpty);
    expect(webpage.items, everyElement(isNotNull));
    webpage.items.forEach((preview) {
      expect(preview.title, isNotNull);
      expect(preview.title, isNotEmpty);
      expect(preview.lastChapter, isNotNull);
      expect(preview.lastChapter, isNotEmpty);
      expect(preview.link, isNotNull);
      expect(preview.lastChapterUrl, isNotNull);
      expect(preview.previewUrl, isNotNull);
    });
  });

  test("Chapter scraping", () async {
    final response = await client.get<String>(
      "http://mangafox.pro/manga/apotheosis",
    );
    final manga = parser.scrapeFromMangaPage(response.data);
    expect(manga, isNotNull);
  });

  test("Tags scraping", () async {
    var u = Uri.parse(
        "https://avt.mkklcdnv3.com/avatar_225/836-tate_no_yuusha_no_nariagari.jpg");
    u.replace(scheme: "http");
  });
}
